﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPathBehaviour : Behaviour {
    
    public override eBehaviourResult Execute(AIAgent agent, float deltaTime) {
        var path = new Pathfinding.List();
        if (agent.GetBlackboard().Get("Path", ref path) == false || path.Length == 0) {
            return eBehaviourResult.FAILURE;
        }

        float speed = 0;
        agent.GetBlackboard().Get("Speed", ref speed);

        Vector3 position;
        position = agent.gameObject.transform.position;

        var first = path.First();

        // Distance to first
        //float distance = Vector3.Distance(first.Position, position);
        float xDiff = first.Position.x - position.x;
        float zDiff = first.Position.z - position.z;
        float distance = Mathf.Sqrt(xDiff * xDiff + zDiff * zDiff);

        // If not at target, move towards target
        if (distance > 2) {
            xDiff /= distance;
            zDiff /= distance;

            // Move to target
            Vector3 movement = new Vector3(xDiff * speed * deltaTime, 0f, zDiff * speed * deltaTime);
            agent.gameObject.transform.Translate(movement);
        }
        else {
            // Remove first node
            path.RemoveFirst();
        }

        return eBehaviourResult.SUCCESS;
    }
}
