﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AIAgent))]
public class PlayerTank : MonoBehaviour {
    private AIAgent m_agent;
    private KeyboardBehaviour m_keyboardBehaviour;

	// Use this for initialization
	public void Start () {
        m_agent = GetComponent<AIAgent>();
        //m_keyboardBehaviour = new KeyboardBehaviour();
	    m_keyboardBehaviour = ScriptableObject.CreateInstance<KeyboardBehaviour>();

        m_agent.AddBehaviour(m_keyboardBehaviour);
	}
	
	// Update is called once per frame
	void Update () {
        m_agent.UpdateAgent(Time.deltaTime);
	}
}
