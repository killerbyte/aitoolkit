﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AIAgent))]
[RequireComponent(typeof(FiniteStateMachine))]
public class FSMEnemyTank : MonoBehaviour {
    private AIAgent m_agent;
    private FiniteStateMachine m_guardFSM;
    [SerializeField]
    private AIAgent m_player;
    [SerializeField]
    private GameObject[] m_waypoints;

    private float attackRange = 20;

    // Use this for initialization
    void Start () {
        m_agent = GetComponent<AIAgent>();
        m_guardFSM = GetComponent<FiniteStateMachine>();

        m_agent.AddBehaviour(m_guardFSM);

        //gameObject.AddComponent<AttackState>();
        //var attackState = GetComponent<AttackState>();
        //attackState.Init(m_player, 10);
        //gameObject.AddComponent<IdleState>();
        //var idleState = GetComponent<IdleState>();
        //idleState.Init();
        //gameObject.AddComponent<PatrolState>();
        //var patrolState = GetComponent<PatrolState>();
        //patrolState.Init(5);

        //// Add waypoints to patrol state
        //foreach (var waypoint in m_waypoints) {
        //    patrolState.AddWaypoint(waypoint.transform.position);
        //}

        //// Transition Conditions
        //float attackTimer = attackState.GetTimer();
        //float idleTimer = idleState.GetTimer();
        //Condition attackTimerCondition = new FloatGreaterCondition(attackState.GetTimerPtr(), 5);
        //Condition idleTimerCondition = new FloatGreaterCondition(idleTimer, 2);
        //Condition distanceCondition = new WithinRangeCondition(m_player, attackRange);
        //Condition outsideRangeCondition = new NotCondition(distanceCondition);

        //// Transitions for states
        //var attackToIdleTransition = new Transition(idleState, outsideRangeCondition);
        //var toAttackTransition = new Transition(attackState, distanceCondition);
        //var idleToPatrolTransition = new Transition(patrolState, idleTimerCondition);

        //// Add Transitions to States
        //attackState.AddTransition(attackToIdleTransition);
        //idleState.AddTransition(idleToPatrolTransition);
        //idleState.AddTransition(toAttackTransition);
        //patrolState.AddTransition(toAttackTransition);

        //// Add States to FSM
        //m_guardFSM.AddState(attackState);
        //m_guardFSM.AddState(idleState);
        //m_guardFSM.AddState(patrolState);

        //// Add Conditions to FSM
        //m_guardFSM.AddCondition(attackTimerCondition);
        //m_guardFSM.AddCondition(idleTimerCondition);
        //m_guardFSM.AddCondition(distanceCondition);
        //m_guardFSM.AddCondition(outsideRangeCondition);

        //// Add Transitions to FSM
        //m_guardFSM.AddTransition(attackToIdleTransition);
        //m_guardFSM.AddTransition(toAttackTransition);
        //m_guardFSM.AddTransition(idleToPatrolTransition);

        //// Set Initial State
        //m_guardFSM.SetInitialState(patrolState);
	}
	
	// Update is called once per frame
	void Update () {
        //m_agent.UpdateAgent(Time.deltaTime);
        Debug.Log(m_guardFSM.GetCurrentState().GetTimer());
	}

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
}
