﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AIAgent))]
[RequireComponent(typeof(KeyboardBehaviour))]
public class FSMPlayerTank : MonoBehaviour {
    private AIAgent m_agent;
    private KeyboardBehaviour m_keyboardBehaviour;

	// Use this for initialization
	void Start () {
        m_agent = GetComponent<AIAgent>();
        m_keyboardBehaviour = GetComponent<KeyboardBehaviour>();

        m_agent.AddBehaviour(m_keyboardBehaviour);
	}
	
	// Update is called once per frame
	void Update () {
        m_agent.UpdateAgent(Time.deltaTime);
	}
}
