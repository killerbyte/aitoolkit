﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AIAgent))]
[RequireComponent(typeof(FollowBehaviour))]
public class EnemyTank : MonoBehaviour {
    private AIAgent m_agent;
    private FollowBehaviour m_followBehaviour;

    // Use this for initialization
    void Start () {
        m_agent = GetComponent<AIAgent>();
        m_followBehaviour = GetComponent<FollowBehaviour>();

        m_followBehaviour.SetSpeed(5);
        m_agent.AddBehaviour(m_followBehaviour);
	}
	
	// Update is called once per frame
	void Update () {
        m_agent.UpdateAgent(Time.deltaTime);
	}
}
