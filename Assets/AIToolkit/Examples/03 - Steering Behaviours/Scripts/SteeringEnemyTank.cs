﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AIAgent))]
public class SteeringEnemyTank : MonoBehaviour {
    private AIAgent m_agent;

    public AIAgent GetAgent() {
        return m_agent;
    }

	// Use this for initialization
	public void Start() {
        m_agent = GetComponent<AIAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        // Check state
        foreach (var behaviour in m_agent.GetBehaviours()) {
            if (behaviour.GetType() == typeof(FiniteStateMachine)) {
                FiniteStateMachine fsm = behaviour as FiniteStateMachine;
                
                if (fsm.GetCurrentState().GetType() == typeof(SteeringState)) {
                    Vector2 velocity = Vector2.zero;
                    m_agent.GetBlackboard().Get("NewVelocity", ref velocity);

                    Vector3 movement = new Vector3(velocity.x * Time.deltaTime,
                        0f, velocity.y * Time.deltaTime);
                    Debug.Log(movement);
                    GetComponent<Rigidbody>().transform.Translate(movement);
                }
            }
        }
	}
}
