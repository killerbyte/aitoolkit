﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    [SerializeField]
    private AIAgent m_player;

    [SerializeField]
    private SteeringEnemyTank[] m_enemies;
    [SerializeField]
    private Obstacle[] m_obstacles;
    private FiniteStateMachine m_fsm;

    private SteeringBehaviour m_steeringBehaviour;
    private SeekForce m_seek;
    private FleeForce m_flee;
    private WanderForce m_wander;
    private ObstacleAvoidanceForce m_avoid;
    private float attackRange = 30f;

    // Use this for initialization
    void Start () {
        m_fsm = new FiniteStateMachine();
        m_seek = new SeekForce();
        m_flee = new FleeForce();
        m_wander = new WanderForce();
        m_avoid = new ObstacleAvoidanceForce();

        // Setup steering properties
        m_seek.SetTarget(m_player);
        m_flee.SetTarget(m_player);
        m_avoid.SetFeelerLength(50);

        SteeringState m_attackState = new SteeringState();
        m_attackState.AddForce(m_wander, .2f);
        m_attackState.AddForce(m_seek, .8f);
        m_attackState.AddForce(m_avoid, 2);

        SteeringState m_wanderState = new SteeringState();
        m_wanderState.AddForce(m_wander, 1);
        m_wanderState.AddForce(m_avoid, 2);

        var withinRangeCondition = new WithinRangeCondition(m_player, attackRange);
        var notWithinRangeCondition = new NotCondition(withinRangeCondition);

        Transition m_withinRange = new Transition(m_attackState, withinRangeCondition);
        Transition m_outsideRange = new Transition(m_wanderState, notWithinRangeCondition);

        m_attackState.AddTransition(m_outsideRange);
        m_wanderState.AddTransition(m_withinRange);

        // Add States
        m_fsm.AddState(m_attackState);
        m_fsm.AddState(m_wanderState);

        // Add Conditions
        m_fsm.AddCondition(withinRangeCondition);
        m_fsm.AddCondition(notWithinRangeCondition);

        // Add Transitions
        m_fsm.AddTransition(m_withinRange);
        m_fsm.AddTransition(m_outsideRange);

        m_fsm.SetInitialState(m_wanderState);

        // Enemy
        //int i = 0;
        foreach (var enemy in m_enemies) {
            enemy.Start();
            enemy.GetAgent().AddBehaviour(m_fsm);

            Vector2 v = new Vector2();
            v.x = 0;
            v.y = 0;

            WanderData wd = new WanderData();
            wd.offset = 100;
            wd.radius = 75;
            wd.jitter = 25;
            wd.x = 0;
            wd.y = 0;

            enemy.GetAgent().GetBlackboard().Set("WanderData", wd);
            enemy.GetAgent().GetBlackboard().Set("Velocity", v);
            enemy.GetAgent().GetBlackboard().Set("MaxForce", 200f);
            enemy.GetAgent().GetBlackboard().Set("MaxVelocity", 100f);
        }
    }
	
	// Update is called once per frame
	void Update () {
        foreach (var enemy in m_enemies) {
            enemy.GetAgent().UpdateAgent(Time.deltaTime);
        }

        // TODO: Add some additional input for controlling the weight of the forces
	}

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;

        foreach (var enemy in m_enemies) {
            Gizmos.DrawWireSphere(enemy.gameObject.transform.position, attackRange);
        }
    }
}
