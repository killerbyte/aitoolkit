﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTreeGameManager : MonoBehaviour {
    [SerializeField]
    private AIAgent m_player;

    [SerializeField]
    private AIAgent m_enemy;

    [SerializeField]
    private float seekRange = 20f;

    [SerializeField]
    private float attackRange = 5f;

    private Behaviour m_guardBehaviour;

    // Use this for initialization
    void Start() {
        Vector2 v = new Vector2();
        v.x = 0;
        v.y = 0;

        WanderData wd = new WanderData();
        wd.offset = 100;
        wd.radius = 75;
        wd.jitter = 25;
        wd.x = 0;
        wd.y = 0;

        m_enemy.GetBlackboard().Set("WanderData", wd);
        m_enemy.GetBlackboard().Set("Velocity", v);
        m_enemy.GetBlackboard().Set("MaxForce", 200f);
        m_enemy.GetBlackboard().Set("MaxVelocity", 100f);

        // Behaviours
        //var obstacleForce = new ObstacleAvoidanceForce();
        //obstacleForce.SetFeelerLength(80);

        var wanderingBehaviour = new SteeringBehaviour();
        wanderingBehaviour.AddForce(new WanderForce());
        //wanderingBehaviour.AddForce(obstacleForce);

        var attackingBehaviour = new SteeringBehaviour();
        //attackingBehaviour.AddForce(new SeekForce(m_player), 0.8f);
        attackingBehaviour.AddForce(new SeekForce(m_player));
        //attackingBehaviour.AddForce(obstacleForce);

        var attackLogDecorator = new LogDecorator(attackingBehaviour, "Now attacking player!");
        var wanderLogDecorator = new LogDecorator(wanderingBehaviour, "Now wandering around!");

        // Conditions for behaviour tree
        var seekCondition = new WithinRangeCondition(m_player, seekRange);
        var attackCondition = new WithinRangeCondition(m_player, attackRange);

        // Behaviour Tree Branches
        var guardBehaviour = new SelectorBehaviour();
        var withinRangeBehaviour = new SequenceBehaviour();

        // Construct the Behaviour Tree
        guardBehaviour.AddChild(attackCondition);
        guardBehaviour.AddChild(withinRangeBehaviour);

            // AND Sequence
            withinRangeBehaviour.AddChild(seekCondition);
            withinRangeBehaviour.AddChild(attackLogDecorator);

        guardBehaviour.AddChild(wanderLogDecorator);

        m_enemy.AddBehaviour(guardBehaviour);

        m_guardBehaviour = guardBehaviour;
    }

    // Update is called once per frame
    void Update() {
        m_player.UpdateAgent(Time.deltaTime);
        m_enemy.UpdateAgent(Time.deltaTime);

        //Debug.Log(m_enemy.movement);
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(m_enemy.transform.position, seekRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(m_enemy.transform.position, attackRange);
    }
}
