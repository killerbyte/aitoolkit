﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionGameManager : MonoBehaviour {
    [SerializeField]
    private AIAgent m_player;

    [SerializeField]
    private AIAgent m_enemy;

    [SerializeField]
    private float seekRange = 20f;

    [SerializeField]
    private float attackRange = 5f;

    private DecisionBehaviour m_enemyDecisions;

	// Use this for initialization
	void Start () {
        m_enemyDecisions = new DecisionBehaviour();

        m_enemy.AddBehaviour(m_enemyDecisions);

        Vector2 v = new Vector2();
        v.x = 0;
        v.y = 0;

        WanderData wd = new WanderData();
        wd.offset = 100;
        wd.radius = 75;
        wd.jitter = 25;
        wd.x = 0;
        wd.y = 0;

        m_enemy.GetBlackboard().Set("WanderData", wd);
        m_enemy.GetBlackboard().Set("Velocity", v);
        m_enemy.GetBlackboard().Set("MaxForce", 100f);
        m_enemy.GetBlackboard().Set("MaxVelocity", 50f);

        // Branches
        var attackOrSeekBranch = new ConditionalDecision();
        var rootBranch = new ConditionalDecision();

        // Decision Tree
        rootBranch.SetCondition(new WithinRangeCondition(m_player, seekRange));
            rootBranch.SetTrueBranch(attackOrSeekBranch);
                attackOrSeekBranch.SetCondition(new WithinRangeCondition(m_player, attackRange));
                    attackOrSeekBranch.SetTrueBranch(new AttackDecision());
                    attackOrSeekBranch.SetFalseBranch(new SteeringDecision(new SeekForce(m_player)));
            rootBranch.SetFalseBranch(new SteeringDecision(new WanderForce()));

        m_enemyDecisions.SetDecision(rootBranch);
    }
	
	// Update is called once per frame
	void Update () {
        m_player.UpdateAgent(Time.deltaTime);
        m_enemy.UpdateAgent(Time.deltaTime);
	}

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(m_enemy.transform.position, seekRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(m_enemy.transform.position, attackRange);
    }
}
