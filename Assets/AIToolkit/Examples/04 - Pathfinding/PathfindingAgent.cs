﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PathBehaviour))]
public class PathfindingAgent : AIAgent {
    private PathBehaviour m_pathBehaviour;

    private Pathfinding.Node m_start;
    private Pathfinding.Node m_end;

    private Pathfinding.List m_path;

    [SerializeField]
    private float m_speed;

    [SerializeField]
    private PathfindingGameManager gameManager;
    
	// Use this for initialization
	new void Awake () {
        gameManager.Awake();
        base.Awake();
        Pathfinding.List nodeMap = gameManager.GetMap();
        m_start = nodeMap.Retrieve(Random.Range(0, nodeMap.Length));
        m_end = nodeMap.Retrieve(Random.Range(0, nodeMap.Length));
        m_path = new Pathfinding.List();
        m_pathBehaviour = GetComponent<PathBehaviour>();

        Pathfinding.Search.AStar(m_start, m_end, m_path, Pathfinding.Search.HeuristicDistanceSqr);

        GetBlackboard().Set("Path", m_path);
        GetBlackboard().Set("Speed", m_speed);

        AddBehaviour(m_pathBehaviour);
        m_pathBehaviour.Nodes = nodeMap;

        // Move the agent to the start position
        gameObject.transform.position = m_start.Position;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateAgent(Time.deltaTime);
	}

    void OnDrawGizmos() {
        // Make sure the application is actually running either in editor or at runtime
        if (Application.isPlaying) {
            foreach (Pathfinding.Node node in m_path) {
                if (node == m_start) {
                    Gizmos.color = Color.green;
                    Gizmos.DrawSphere(node.Position, 3f);
                }
                else if (node == m_end) {
                    Gizmos.color = Color.red;
                    Gizmos.DrawSphere(node.Position, 3f);
                }
                else {
                    Gizmos.color = Color.magenta;
                    Gizmos.DrawSphere(node.Position, 3f);
                }
            }
        }
    }
}
