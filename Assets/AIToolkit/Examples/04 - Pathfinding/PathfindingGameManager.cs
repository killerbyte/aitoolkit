﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingGameManager : MonoBehaviour {
    private Pathfinding.List m_nodes;

    [SerializeField]
    private GameObject nodeMap;

	// Use this for initialization
	public void Awake () {
        m_nodes = new Pathfinding.List();

        // Create nodes
        for (int x = 0; x < 50; ++x) {
            for (int z = 0; z < 25; ++z) {
                if (Random.Range(0, 100) < 30) continue;

                //GameObject gameNode = new GameObject("Node");
                //gameNode.transform.SetParent(nodeMap.transform);

                Pathfinding.Node node = new Pathfinding.Node();
                node.Position = new Vector3(x * 10, 0f, z * 10);
                //gameNode.transform.position = new Vector3(node.Position.x, 1f, node.Position.z);

                m_nodes.Add(node);
            }
        }

        // Create edges
        foreach (Pathfinding.Node a in m_nodes) {
            foreach (Pathfinding.Node b in m_nodes) {
                if (a == b) continue;

                float x = b.Position.x - a.Position.x;
                float z = b.Position.z - a.Position.z;
                float sqrDist = x * x + z * z;

                if (sqrDist <= (20 * 20)) {
                    var edge = new Pathfinding.Edge();
                    edge.Cost = sqrDist;
                    edge.Target = b;

                    a.Edges.Add(edge);
                }
            }
        }

        //// Create Medkits
        //for (int i = 0; i < 5; ++i) {
        //    var node = m_nodes[Random.Range(0, m_nodes.Count)];
        //    node.Flags |= Pathfinding.Node.eFlags.MEDKIT;
        //}
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public Pathfinding.List GetMap() {
        return m_nodes;
    }

    void OnDrawGizmos() {
        // Make sure the application is actually running either in editor or at runtime
        if (Application.isPlaying) {
            // Draw all nodes
            foreach (Pathfinding.Node node in m_nodes) {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(node.Position, 2f);

                // Draw Edges
                Gizmos.color = Color.yellow;
                foreach (Pathfinding.Edge edge in node.Edges) {
                    Pathfinding.Node target = edge.Target;
                    Gizmos.DrawLine(node.Position, target.Position);
                }
            }
        }
    }
}
