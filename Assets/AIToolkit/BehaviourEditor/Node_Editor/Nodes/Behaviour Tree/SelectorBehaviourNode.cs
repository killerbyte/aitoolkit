﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeEditorFramework.Utilities;

namespace NodeEditorFramework.Standard {
    [System.Serializable]
    [Node(false, "Behaviour/Selector Behaviour")]
    public class SelectorBehaviourNode : Node {

        public const string ID = "selectorBehaviourNode";
        public override string GetID { get { return ID; } }

        public Behaviour[] input = null;
        public int count = 0;
        public int prevCount = 0;

        private SelectorBehaviourNode node = null;

        public override Node Create(Vector2 pos) {
            node = CreateInstance<SelectorBehaviourNode>();

            node.name = "Selector Behaviour Node";
            node.rect = new Rect(pos.x, pos.y, 200, 100);

            node.CreateInput("Input 1", "Integer");
            for (int i = 0; i < count; i++) {
                node.CreateOutput("Output " + i, "Behaviour");
            }

            return node;
        }

        void Update() {
            // Figure out a way for deleting the outputs
            //Outputs.Clear();
            for (int i = 0; i < Outputs.Count; i++) {
                Outputs[i].Delete();
            }

            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    CreateOutput("Output " + i, "Behaviour");
                }
            }

            // Resize the rect if necessary - Need to fix this so it incrementally increases
            if (count % 4 == 0 && count > 0) {
                //rect.height *= (2 * (count / 4));
            }
        }

        protected internal override void NodeGUI() {
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();

            if (Inputs[0].connection != null)
                GUILayout.Label(Inputs[0].name);
            else
                count = RTEditorGUI.IntField("Count: ", count, null);
            InputKnob(0);
            // --
            if (prevCount != count) {
                prevCount = count;
                Update();
            }

            GUILayout.EndVertical();
            GUILayout.BeginVertical();

            // Dynamic Behaviour List OUTPUTS
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    Outputs[i].DisplayLayout();
                }
            }

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            if (GUI.changed)
                NodeEditor.RecalculateFrom(this);
        }
    }
}
