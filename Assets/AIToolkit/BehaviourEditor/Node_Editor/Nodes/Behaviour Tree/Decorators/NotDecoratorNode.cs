﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeEditorFramework.Utilities;

namespace NodeEditorFramework.Standard {
    [System.Serializable]
    [Node(false, "Behaviour/Decorators/Not Decorator")]
    public class NotDecoratorNode : Node {
        
        public const string ID = "notDecoratorNode";
        public override string GetID { get { return ID; } }

        public Behaviour input = null;

        public override Node Create(Vector2 pos) {
            NotDecoratorNode node = CreateInstance<NotDecoratorNode>();

            node.name = "Not Decorator Node";
            node.rect = new Rect(pos.x, pos.y, 200, 100);

            node.CreateInput("Input 1", "Behaviour");

            node.CreateOutput("Output 1", "Behaviour");

            return node;
        }

        protected internal override void NodeGUI() {
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();

            if (Inputs[0].connection != null)
                GUILayout.Label(Inputs[0].name);
            else
                input = RTEditorGUI.ObjectField<Behaviour>(input, false);
            InputKnob(0);

            GUILayout.EndVertical();
            GUILayout.BeginVertical();

            Outputs[0].DisplayLayout();

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            if (GUI.changed)
                NodeEditor.RecalculateFrom(this);
        }
    }
}