﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeEditorFramework.Utilities;

namespace NodeEditorFramework.Standard {
    [System.Serializable]
    [Node(false, "Behaviour/Decorators/Log Decorator")]
    public class LogDecoratorNode : Node {

        public const string ID = "logDecoratorNode";
        public override string GetID { get { return ID; } }

        public Behaviour input = null;
        public string strInput = null;

        public override Node Create(Vector2 pos) {
            LogDecoratorNode node = CreateInstance<LogDecoratorNode>();

            node.name = "Log Decorator Node";
            node.rect = new Rect(pos.x, pos.y, 200, 100);

            node.CreateInput("Input 1", "Behaviour");
            node.CreateInput("Input 2", "String");

            node.CreateOutput("Output 1", "Behaviour");

            return node;
        }

        protected internal override void NodeGUI() {
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();

            if (Inputs[0].connection != null)
                GUILayout.Label(Inputs[0].name);
            else
                input = RTEditorGUI.ObjectField<Behaviour>(input, false);
            InputKnob(0);
            // --
            if (Inputs[1].connection != null)
                GUILayout.Label(Inputs[1].name);
            else
                strInput = RTEditorGUI.TextField(new GUIContent("Log Message", 
                    "Message to be displayed in the console"), 
                    strInput, null);
            InputKnob(1);

            GUILayout.EndVertical();
            GUILayout.BeginVertical();

            Outputs[0].DisplayLayout();

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            if (GUI.changed)
                NodeEditor.RecalculateFrom(this);
        }
    }
}