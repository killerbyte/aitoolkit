﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeEditorFramework.Utilities;
using System;

namespace NodeEditorFramework.Standard {
    // Connection Type only for visual purposes
    public class BehaviourType : IConnectionTypeDeclaration {
        public string Identifier { get { return "Behaviour"; } }
        public Type Type { get { return typeof(Behaviour); } }
        public Color Color { get { return Color.red; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }

    public class StringType : IConnectionTypeDeclaration {
        public string Identifier { get { return "String"; } }
        public Type Type { get { return typeof(string); } }
        public Color Color { get { return Color.red; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }

    public class IntType : IConnectionTypeDeclaration {
        public string Identifier { get { return "Integer"; } }
        public Type Type { get { return typeof(int); } }
        public Color Color { get { return Color.cyan; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }
}
