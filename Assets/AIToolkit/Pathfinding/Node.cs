﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pathfinding {
    public class Node : MonoBehaviour {
        private List<Pathfinding.Edge> edges;
        private int flags;

        private float gScore;
        private float hScore;
        private float fScore;
        private Node previousNode;

        public enum eFlags {
            MEDKIT = (1 << 0),
        }

        public Node() {
            flags = 0;
            edges = new List<Edge>();
        }

        public int Flags {
            get {
                return flags;
            }

            set {
                flags = value;
            }
        }

        public List<Pathfinding.Edge> Edges {
            get {
                return edges;
            }

            set {
                edges = value;
            }
        }

        public float GScore {
            get {
                return gScore;
            }

            set {
                gScore = value;
            }
        }

        public float HScore {
            get {
                return hScore;
            }

            set {
                hScore = value;
            }
        }

        public float FScore {
            get {
                return fScore;
            }

            set {
                fScore = value;
            }
        }

        public Node Previous {
            get {
                return previousNode;
            }

            set {
                previousNode = value;
            }
        }

        [SerializeField]
        public Vector3 Position {
            get {
                return gameObject.transform.position;
            }

            set {
                gameObject.transform.position = value;
            }
        }

        static bool CompareGScore(Node a, Node b) {
            return a.gScore < b.gScore;
        }

        void OnDrawGizmos() {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(Position, 10f);
        }
    }
}