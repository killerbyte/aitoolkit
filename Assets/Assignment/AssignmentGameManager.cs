﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignmentGameManager : MonoBehaviour {
    [SerializeField]
    private AIAgent m_player;

    [SerializeField]
    private AIAgent m_enemy;

    [SerializeField]
    private AIAgent[] m_enemies;

    [SerializeField]
    private float seekRange = 100f;

    [SerializeField]
    private float attackRange = 30f;

    [SerializeField]
    private float speed;

    private Behaviour m_guardBehaviour;

    [SerializeField]
    private GameObject nodeMap; // Children are nodes

    private Pathfinding.List m_nodes;

    // Use this for initialization
    void Start() {
        CreateNodeMap();

        // Behaviours
        var followPath = ScriptableObject.CreateInstance<FollowPathBehaviour>();
        var newPath = ScriptableObject.CreateInstance<NewPathBehaviour>();
        newPath.SetNodes(m_nodes);
        var playerPath = ScriptableObject.CreateInstance<PlayerPathBehaviour>();
        playerPath.SetNodes(m_nodes);

        //var playerLogDecorator = new LogDecorator(playerPath, "Now Attacking Player");
        var playerPathDecorator = ScriptableObject.CreateInstance<PlayerPathDecorator>();
        playerPathDecorator.Init(followPath, m_nodes);
        //var newPathLogDecorator = new LogDecorator(newPath, "Following New Path");
        var newPathLogDecorator = ScriptableObject.CreateInstance<LogDecorator>();
        newPathLogDecorator.Init(newPath, "Following New Path");

        // Conditions for behaviour tree
        //var seekCondition = new WithinRangeCondition(m_player, seekRange);
        var seekCondition = ScriptableObject.CreateInstance<WithinRangeCondition>();
        seekCondition.Init(m_player, seekRange);
        //var attackCondition = new WithinRangeCondition(m_player, attackRange);
        var attackCondition = ScriptableObject.CreateInstance<WithinRangeCondition>();
        attackCondition.Init(m_player, attackRange);

        // Behaviour Tree Branches
        //var guardBehaviour = new SelectorBehaviour();
        var guardBehaviour = ScriptableObject.CreateInstance<SelectorBehaviour>();
        var withinRangeBehaviour = ScriptableObject.CreateInstance<SequenceBehaviour>();
        var pathFollowBehaviour = ScriptableObject.CreateInstance<SelectorBehaviour>();
        //var seekingBehaviour = new SelectorBehaviour();

        // Construct the Behaviour Tree
        guardBehaviour.AddChild(attackCondition);
        guardBehaviour.AddChild(withinRangeBehaviour);

            // AND Sequence
            withinRangeBehaviour.AddChild(seekCondition);
            withinRangeBehaviour.AddChild(playerPathDecorator);

                //seekingBehaviour.AddChild(followPath);
                //seekingBehaviour.AddChild(playerLogDecorator);
                //seekingBehaviour.AddChild(playerLogDecorator);
                //seekingBehaviour.AddChild(playerPathDecorator);
                //seekingBehaviour.AddChild(followPath);
                
        guardBehaviour.AddChild(pathFollowBehaviour);

            pathFollowBehaviour.AddChild(followPath);
            pathFollowBehaviour.AddChild(newPathLogDecorator);

        //m_enemy.AddBehaviour(guardBehaviour);
        foreach (var enemy in m_enemies) {
            // Create the path
            Pathfinding.List path = new Pathfinding.List();
            //var start = m_nodes.Retrieve(Random.Range(0, m_nodes.Length));
            var pos = enemy.gameObject.transform.position;
            var start = Pathfinding.Search.FindClosest(pos.x, pos.z, m_nodes);
            var end = m_nodes.Retrieve(Random.Range(0, m_nodes.Length));
            Pathfinding.Search.AStar(start, end, path, Pathfinding.Search.HeuristicDistanceSqr);

            // Set Blackboard Variables
            enemy.GetBlackboard().Set("Path", path);
            enemy.GetBlackboard().Set("Speed", speed);

            enemy.AddBehaviour(guardBehaviour);
        }

        m_guardBehaviour = guardBehaviour;
    }

    // Update is called once per frame
    void Update() {
        m_player.UpdateAgent(Time.deltaTime);
        //m_enemy.UpdateAgent(Time.deltaTime);

        foreach (var enemy in m_enemies) {
            enemy.UpdateAgent(Time.deltaTime);
        }

        //Debug.Log(m_enemy.movement);
    }

    //void OnDrawGizmos() {
    //    foreach (var enemy in m_enemies) {
    //        Gizmos.color = Color.yellow;
    //        Gizmos.DrawWireSphere(enemy.transform.position, seekRange);

    //        Gizmos.color = Color.red;
    //        Gizmos.DrawWireSphere(enemy.transform.position, attackRange);
    //    }
    //}

    void CreateNodeMap() {
        // Retrieve the nodes from the node map
        m_nodes = new Pathfinding.List();

        Pathfinding.Node[] nodes = nodeMap.GetComponentsInChildren<Pathfinding.Node>();
        foreach (var node in nodes) {
            m_nodes.Add(node);
        }

        // Create the edges
        foreach (Pathfinding.Node a in m_nodes) {
            foreach (Pathfinding.Node b in m_nodes) {
                if (a == b) continue;

                float x = b.Position.x - a.Position.x;
                float z = b.Position.z - a.Position.z;
                float sqrDist = x * x + z * z;

                if (sqrDist <= (35 * 35)) {
                    var edge = new Pathfinding.Edge();
                    edge.Cost = sqrDist;
                    edge.Target = b;

                    a.Edges.Add(edge);
                }
            }
        }
    }
}
